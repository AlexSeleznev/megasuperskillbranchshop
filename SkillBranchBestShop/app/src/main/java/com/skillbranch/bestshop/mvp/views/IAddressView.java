package com.skillbranch.bestshop.mvp.views;

import com.skillbranch.bestshop.data.storage.dto.UserAddressDto;

public interface IAddressView extends IView {
    void showInputError();

    UserAddressDto getUserAddress();
    void goBackToParentScreen();
}
