package com.skillbranch.bestshop.mvp.presenters;

public interface IAddressPresenter {
    void clickOnAddAddress();
    void goBackToParentScreen();
}
