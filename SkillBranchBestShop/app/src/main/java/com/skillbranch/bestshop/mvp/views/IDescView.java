package com.skillbranch.bestshop.mvp.views;

import com.skillbranch.bestshop.data.storage.dto.DescriptionDto;

public interface IDescView extends IView {
    public void initView(DescriptionDto descriptionDto);
}
