package com.skillbranch.bestshop.mvp.models;

import com.birbit.android.jobqueue.JobManager;

import javax.inject.Inject;

import com.skillbranch.bestshop.data.managers.DataManager;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.di.components.DaggerModelComponent;
import com.skillbranch.bestshop.di.components.ModelComponent;
import com.skillbranch.bestshop.di.modules.ModelModule;

public abstract class AbstractModel {
    @Inject
    DataManager mDataManager;
    @Inject
    JobManager mJobManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if(component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
            //DataManagerComponent
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }


}
